# Praktikum Sistem Operasi 2023

Kelas Sistem Operasi B - Kelompok IT05

### Nama Anggota 
- Oktavia Anggraeni (5027211001)
- Aqil Suthan Yuki Maye (5027211007)
- Sulthan Akmal Rafliansyah (5027211039)
## Shift Modul 2
### Soal 1


### Soal 2


### Soal 3


### Soal 4
Membuat program untuk menjalankan script bash yang menyerupai
crontab dan menggunakan bahasa C <br>
Dengan ketentuan: <br>
**a.** Tidak menggunakan fungsi system() <br>
**b.** Dalam pelatihan fokus time managementnya, program harus dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh. <br>
**c.** Dalam pelatihan fokus untuk ketepatan pilihannya, program dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati, terserah bagaimana, yang penting tulisan error. <br>
**d.** Terakhir, dalam pelatihan kesempurnaan fokusnya, program ini diharapkan dapat berjalan dalam background dan hanya menerima satu config cron. <br>
**Bonus** Bonus poin apabila CPU state minimum <br>

Penyelesaian:
#### mainan.c
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define MAX_LEN 1024

void print_run() {
    printf("Error: Argumen tidak valid. Gunakan: ./mainan \* HH MM SS path/to/script.sh &\n");
}

int main(int argc, char *argv[]) {
    if (argc != 6) {
        print_run();
        return 1;
    }
    
    // Memisahkan argumen
    char *script_path = argv[5];
    int HH, MM, SS; // Deklarasi jam, menit, detik
    if (strcmp(argv[1], "*") == 0) {
        HH = -1;
    } 
    else {
    	// konversi argumen 1 sebagai jam (HH)
        HH = atoi(argv[1]);
        if (HH < 0 || HH > 23) {
            printf("Error: Argumen tidak valid.\n");
            return 1;
        }
    }
    // konversi argumen 2 dan 3 sebagai menit (MM) dan detik (SS)
    MM = atoi(argv[2]);
    SS = atoi(argv[3]);
    if (MM < 0 || MM > 59 || SS < 0 || SS > 59) {
        printf("Error: Argumen menit atau detik tidak valid.\n");
        return 1;
    }

    // Menghitung sleep time
    time_t now = time(NULL);
    struct tm *now_tm = localtime(&now);
    int sleep_time;
    if (HH == -1) {
        // eksekusi script setiap menit
        sleep_time = 60 - now_tm->tm_sec;
    } else {
        // eksekusi script pada waktu spesifik
        if (now_tm->tm_hour > HH || (now_tm->tm_hour == HH && now_tm->tm_min > MM) || (now_tm->tm_hour == HH && now_tm->tm_min == MM && now_tm->tm_sec >= SS)) {
            // kondisi saat waktu yang ditentukan telah berlalu, maka akan dilakukan besok
            sleep_time = 24 * 60 * 60 - (now_tm->tm_hour * 60 * 60 + now_tm->tm_min * 60 + now_tm->tm_sec) + HH * 60 * 60 + MM * 60 + SS;
        } else {
            // kondisi saat waktu yang ditentukan belum berlalu, masih pada hari yang sama
            sleep_time = (HH - now_tm->tm_hour) * 60 * 60 + (MM - now_tm->tm_min) * 60 + (SS - now_tm->tm_sec);
        }
    }
	
    // Fork proses dan jalankan script dalam proses child
    pid_t pid = fork();
    if (pid == -1) {
        printf("Error: Gagal melakukan fork.\n");
        return 1;
    } else if (pid == 0) {
        // Child process
        sleep(sleep_time);
        execlp(script_path, script_path, NULL);
        printf("Error: Gagal mengeksekusi script.\n");
        return 1;
    } else {
        // Parent process
        // background
        printf("Silakan tunggu, script akan dijalankan dalam %d detik.\n", sleep_time);
        return 0;
    }
}
```
Penjelasan:
- Sebelum menjalankan proram ini, pastikan telah membuat file .sh pada direktori path yang sesuai
#### Contoh file script.sh
```
#!/bin/bash

echo "~~~Shell script dipanggil~~~"

```
- Lakukan pengubahan hak akses "execute" pada file mainan.c dan script.sh 
```
chmod +x mainan.c
```
```
chmod +x script.sh 
```
- Kompilasi program mainan.c dengan menggunakan perintah gcc mainan.c -o mainan <br>
- Jalankan program melalui terminal dengan cara berikut: <br>
```
./mainan \* HH MM SS path/to/script.sh &

```
HH merepresentasikan jam dalam format 24 jam. Jika argumen HH berupa "*" maka akan diisi dengan nilai -1, karena program akan dieksekusi setiap menit
MM merepresentasikan menit
SS merepresentasikan detik <br>

- Program akan mengkonversi argumen 1 sebagai jam (HH) mengecek apakah format HH sudah valid, yaitu nilai jam (HH) harus berada di antara 0 hingga 23. Jika HH yang diinput tidak valid maka program akan mengeluarkan output error.
```
    if (strcmp(argv[1], "*") == 0) {
        HH = -1;
    } 
    else {
    	// konversi argumen 1 sebagai jam (HH)
        HH = atoi(argv[1]);
        if (HH < 0 || HH > 23) {
            printf("Error: Argumen tidak valid.\n");
            return 1;
```
- Selanjutnya, fungsi akan mengkonversi argumen ke-2 dan ke-3 sebagai menit (MM) dan detik (SS), dan memeriksa apakah nilai MM dan SS valid, yaitu harus berada di antara 0 hingga 59. Jika ada yang tidak valid makan program akan mengeluarkan output error.
```
    // konversi argumen 2 dan 3 sebagai menit (MM) dan detik (SS)
    MM = atoi(argv[2]);
    SS = atoi(argv[3]);
    if (MM < 0 || MM > 59 || SS < 0 || SS > 59) {
        printf("Error: Argumen menit atau detik tidak valid.\n");
        return 1;
    }
```

- Jika input programnya valid, atau bagian * tidak diisi atau sebagai berikut:
```
./mainan \* * * * path/to/script.sh &
```
maka akan melanjutkan proses untuk sleeptime, yaitu file dieksekusi tiap menit <br>
- Jika waktu yang diinput valid tetapi telah berlalu pada hari yang sama, maka output akan ditampilkan hari besok
```
// kondisi saat waktu yang ditentukan telah berlalu, maka akan dilakukan besok
sleep_time = 24 * 60 * 60 - (now_tm->tm_hour * 60 * 60 + now_tm->tm_min * 60 + now_tm->tm_sec) + HH * 60 * 60 + MM * 60 + SS;
```
- Jika waktu yang diinput valid, program akan menghitung sleep time dengan cara menghitung selisih waktu antara waktu saat ini dengan waktu yang ditentukan pada hari yang sama. Sleep time dihitung dengan cara menghitung selisih waktu antara jam, menit, dan detik pada waktu yang ditentukan dengan jam, menit, dan detik pada waktu saat ini.
```            
// kondisi saat waktu yang ditentukan belum berlalu, masih pada hari yang sama
sleep_time = (HH - now_tm->tm_hour) * 60 * 60 + (MM - now_tm->tm_min) * 60 + (SS - now_tm->tm_sec);

```
- Proses akan berlangsung pada parent proses dengan keterangan "Silakan tunggu, script akan dijalankan dalam %d detik.\n"
