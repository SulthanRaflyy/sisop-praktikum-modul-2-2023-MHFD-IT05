#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>

bool stop_all = false;

//buat nama folder
void generate_folder_name(char* folder_name) {
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);
    strftime(folder_name, 20, "%Y-%m-%d_%H:%M:%S", time_info);
}

//download gambar
void download_image(char* folder_name) {
    char timestamp[20];
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);
    strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", time_info);

    char url[50];
    sprintf(url, "https://picsum.photos/%d/%d", (int)(current_time % 1000) + 50, (int)(current_time % 1000) + 50);

    char file_name[50];
    sprintf(file_name, "%s/%s.jpg", folder_name, timestamp);

    char* args[] = {"wget", "-q", "-O", file_name, url, NULL};
    execvp(args[0], args);
}

//zip folder
void zip_folder(char* folder_name) {
    pid_t child_pid = fork();

    if (child_pid == 0) {
        char zip_command[50];
        sprintf(zip_command, "zip -r %s.zip %s", folder_name, folder_name);
        execlp("sh", "sh", "-c", zip_command, NULL);
    } else {
        wait(NULL);
    }
}

//buat killer
void generate_killer(char* mode) {
    FILE *fp = fopen("killer.c", "w");
    if (fp == NULL) {
        perror("Error opening file");
        exit(1);
    }

    fprintf(fp, "#include <signal.h>\n");
    fprintf(fp, "#include <stdlib.h>\n");
    fprintf(fp, "#include <unistd.h>\n");
    fprintf(fp, "#include <stdio.h>\n\n");
    fprintf(fp, "int main() {\n");

    if (strcmp(mode, "-a") == 0) {
        fprintf(fp, "    kill(0, SIGTERM);\n");
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fp, "    int stop_all = 0;\n");
        fprintf(fp, "    int i;\n");
        fprintf(fp, "    for (i = 0; i < 15; i++) {\n");
        fprintf(fp, "        if (fork() == 0) {\n");
        fprintf(fp, "            while (!stop_all) {\n");
        fprintf(fp, "                sleep(1);\n");
        fprintf(fp, "            }\n");
        fprintf(fp, "            exit(0);\n");
        fprintf(fp, "        }\n");
        fprintf(fp, "    }\n");
    }

    fprintf(fp, "    remove(\"killer\");\n");
    fprintf(fp, "    return 0;\n");
    fprintf(fp, "}");

    fclose(fp);

    pid_t pid = fork();
    if (pid == -1) {
        perror("Error: fork() failed");
        exit(1);
    } else if (pid == 0) { // Child process
        char *args[] = {"gcc", "-o", "killer", "killer.c", NULL};
        execvp(args[0], args);
        perror("Error: execvp() failed");
        exit(1);
    } else { // Parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            remove("killer.c");
        } else {
            perror("Error: gcc compilation failed");
            exit(1);
        }
    }
}

int main(int argc, char *argv[]) {
    char folder_name[20];
    char* mode;
    char* args[] = {"gcc", "-o", "killer", "killer.c", NULL};

    if (argc == 2) {
        if (strcmp(argv[1], "-a") == 0) {
            mode = "-a";
        } else if (strcmp(argv[1], "-b") == 0) {
            mode = "-b";
        } else {
            printf("Invalid argument.\n");
            return 1;
        }
    } else {
        printf("Invalid argument.\n");
        return 1;
    }

    generate_killer(mode);

    // Compile killer.c
    pid_t pid = fork();
    if (pid == 0) {
        execvp(args[0], args);
        perror("execvp failed");
        exit(1);
    } else if (pid < 0) {
        perror("fork failed");
        exit(1);
    }
    wait(NULL);

    while (1) {
        generate_folder_name(folder_name);
        printf("Creating folder: %s\n", folder_name);
        mkdir(folder_name, 0777);

        int i;
        for (i = 0; i < 15; i++) {
            if (fork() == 0) {
                while (!stop_all) {
                    download_image(folder_name);
                    sleep(5);
                }
                exit(0);
            }
            sleep(5);
        }

        for (i = 0; i < 15; i++) {
            wait(NULL);
        }

        zip_folder(folder_name);

        if (mode == "-a") {
            return 0;
        }

        if (mode == "-b") {
            stop_all = true;
            for (i = 0; i < 15; i++) {
                wait(NULL);
            }
        }

        printf("Deleting folder: %s\n", folder_name);
        char* rm_args[] = {"rm", "-rf", folder_name, NULL};
        pid_t rm_pid = fork();
        if (rm_pid == 0) {
            execvp(rm_args[0], rm_args);
            perror("execvp failed");
            exit(1);
        } else if (rm_pid < 0) {
            perror("fork failed");
            exit(1);
        }
        wait(NULL);

        sleep(30);
    }
    return 0;
}
