#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_FILENAME_LENGTH 256
#define NUM_IMAGES 9

int main() {
    
//poin 1

  char *download_command[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
  char *unzip_command[] = {"unzip", "binatang.zip", NULL};

  // Mendownload file binatang.zip dari Google Drive
  printf("Mendownload file binatang.zip dari Google Drive...\n");
  if (fork() == 0) {
    execvp(download_command[0], download_command);
    perror("execvp failed");
    exit(1);
  }
  wait(NULL);

  // Mengekstrak isi file binatang.zip ke dalam folder
  printf("Mengekstrak isi file binatang.zip...\n");
  if (fork() == 0) {
    execvp(unzip_command[0], unzip_command);
    perror("execvp failed");
    exit(1);
  }
  wait(NULL);

  printf("Selesai!\n");

  //poin 2

  DIR *dir;
    struct dirent *ent;
    char *image_filenames[] = {"buaya_amphibi.jpg", "hippo_amphibi.jpg", "komodo_darat.jpg", "lele_air.jpg", "lintah_amphibi.jpg", "paus_air.jpg", "shark_air.jpg", "singa_darat.jpg", "beruang_darat.jpg"};

    // Seed random number generator
    srand(time(NULL));

    // Open directory
    dir = opendir(".");
    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }

    // Count the number of image files in the directory
    int num_images = 0;
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG && strstr(ent->d_name, ".jpg") != NULL) {
            num_images++;
        }
    }

    // Choose a random image filename
    char chosen_filename[MAX_FILENAME_LENGTH];
    if (num_images == 0) {
        printf("No image files found in directory\n");
        exit(EXIT_FAILURE);
    } else {
        strcpy(chosen_filename, image_filenames[rand() % 9]);
    }

    printf("Shift terpilih: %s\n", chosen_filename);

    closedir(dir);

  //poin 3
    char *darat_dirname = "HewanDarat";
    char *amphibi_dirname = "HewanAmphibi";
    char *air_dirname = "HewanAir";

    // Create directories
    mkdir(darat_dirname, 0777);
    mkdir(amphibi_dirname, 0777);
    mkdir(air_dirname, 0777);

    // Open directory
    dir = opendir(".");
    if (dir == NULL) {
        perror("Unable to open directory");
        exit(EXIT_FAILURE);
    }

    // Move image files to appropriate directories
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG && strstr(ent->d_name, ".jpg") != NULL) {
            char *filename = ent->d_name;
            if (strstr(filename, "darat") != NULL) {
                char dest_filename[MAX_FILENAME_LENGTH];
                sprintf(dest_filename, "%s/%s", darat_dirname, filename);
                rename(filename, dest_filename);
            } else if (strstr(filename, "amphibi") != NULL) {
                char dest_filename[MAX_FILENAME_LENGTH];
                sprintf(dest_filename, "%s/%s", amphibi_dirname, filename);
                rename(filename, dest_filename);
            } else if (strstr(filename, "air") != NULL) {
                char dest_filename[MAX_FILENAME_LENGTH];
                sprintf(dest_filename, "%s/%s", air_dirname, filename);
                rename(filename, dest_filename);
            }
        }
    }

    closedir(dir);

  // //poin 4
    // Zip each directory
    printf("Zipping directories...\n");
    char *zip_darat_command[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    char *zip_amphibi_command[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    char *zip_air_command[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    if (fork() == 0) {
        execvp(zip_darat_command[0], zip_darat_command);
        perror("execvp failed");
        exit(1);
    }
    if (fork() == 0) {
        execvp(zip_amphibi_command[0], zip_amphibi_command);
        perror("execvp failed");
        exit(1);
    }
    if (fork() == 0) {
        execvp(zip_air_command[0], zip_air_command);
        perror("execvp failed");
        exit(1);
    }
    wait(NULL);
    wait(NULL);
    wait(NULL);

    // Delete each directory
    printf("Deleting directories...\n");
    char *delete_darat_command[] = {"rm", "-r", "HewanDarat", NULL};
    char *delete_amphibi_command[] = {"rm", "-r", "HewanAmphibi", NULL};
    char *delete_air_command[] = {"rm", "-r", "HewanAir", NULL};
    if (fork() == 0) {
        execvp(delete_darat_command[0], delete_darat_command);
        perror("execvp failed");
        exit(1);
    }
    if (fork() == 0) {
        execvp(delete_amphibi_command[0], delete_amphibi_command);
        perror("execvp failed");
        exit(1);
    }
    if (fork() == 0) {
        execvp(delete_air_command[0], delete_air_command);
        perror("execvp failed");
        exit(1);
    }
    wait(NULL);
    wait(NULL);
    wait(NULL);

    system("rm binatang.zip");

    printf("Selesai!\n");
    return 0;

}
