#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>

#define MANUTD_TEAM "ManUtd"
#define MAX_FILENAME 256

// fungsi untuk mengunduh file players.zip dari Google Drive menggunakan wget
void downloadFile() {
    // array yang berisi argumen-argumen untuk menjalankan perintah wget
    const char* const args[] = {"wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};

    // membuat proses baru menggunakan fork()
    pid_t pid = fork();
    if (pid == -1) { // fork() gagal
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (pid == 0) { // proses anak
        // menjalankan perintah wget menggunakan execvp()
        execvp(args[0], (char* const*) args);
        // jika execvp() gagal, menampilkan pesan error
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    // proses induk
    int status;
    // menunggu proses anak selesai menggunakan waitpid()
    if (waitpid(pid, &status, 0) == -1) { // waitpid() gagal
        perror("waitpid");
        exit(EXIT_FAILURE);
    }
    // jika proses anak selesai dan keluar dengan kode non-zero,
    // menampilkan pesan error dan keluar dari program
    if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        fprintf(stderr, "Aduh gagal download filenya ;((\n");
        exit(EXIT_FAILURE);
    }
}


// Fungsi Extract Database Players
void extractFile() {
    // array yang berisi argumen-argumen untuk menjalankan perintah unzip
    char* args[] = {"unzip", "-q", "players.zip", NULL};

    // membuat proses baru menggunakan fork()
    pid_t pid = fork();
    if (pid == -1) { // fork() gagal
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (pid == 0) { // proses anak
        // menjalankan perintah unzip menggunakan execvp()
        execvp(args[0], args);
        // jika execvp() gagal, menampilkan pesan error
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    // proses induk
    int status;
    // menunggu proses anak selesai menggunakan waitpid()
    if (waitpid(pid, &status, 0) == -1) { // waitpid() gagal
        perror("waitpid");
        exit(EXIT_FAILURE);
    }
    // jika proses anak selesai dan keluar dengan kode non-zero,
    // menampilkan pesan error dan keluar dari program
    if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        fprintf(stderr, "Error extracting players\n");
        exit(EXIT_FAILURE);
    }

    // menghapus file players.zip menggunakan remove()
    if (remove("players.zip") != 0) {
        perror("remove");
    }
}


// Fungsi Filter Players
void filterPlayers() {
    // membuka direktori "players" menggunakan opendir()
    DIR* dir = opendir("players");
    if (dir == NULL) { // jika opendir() gagal
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    struct dirent* entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) { // membaca setiap entri di dalam direktori
        if (entry->d_type == DT_REG) { // jika entri adalah file biasa
            // mengecek apakah nama file sesuai dengan pola yang diberikan menggunakan fnmatch()
            if (fnmatch("*_ManUtd_*_*.png", entry->d_name, 0) != 0) {
                // menghapus file yang tidak sesuai dengan pola menggunakan unlink()
                sprintf(fileName, "%s/%s", "players", entry->d_name);
                if (unlink(fileName) != 0) { // jika unlink() gagal
                    perror("unlink");
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    // menutup direktori menggunakan closedir()
    if (closedir(dir) != 0) { // jika closedir() gagal
        perror("closedir");
        exit(EXIT_FAILURE);
    }
}


void create_directory(const char* dir_name) {
    char* args[] = {
        "mkdir",
        dir_name,
        NULL
    };
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error creating directory: %s\n", dir_name);
            exit(EXIT_FAILURE);
        }
    }
}


void move_file(const char* old_path, const char* new_path, const char* file_name) {
    char* args[] = {
        "mv",
        old_path,
        new_path,
        NULL
    };
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error moving file: %s\n", file_name);
            exit(EXIT_FAILURE);
        }
    }
}

void categorizePlayers() {
    // buka direktori "players" menggunakan opendir()
    DIR* dir = opendir("./players");
    if (dir == NULL) { // jika opendir() gagal
        perror("opendir");
        exit(EXIT_FAILURE);
    }

    // daftar posisi pemain
    const char* positions[] = {
        "Kiper",
        "Bek",
        "Gelandang",
        "Penyerang"
    };

    // buat direktori baru untuk setiap posisi pemain
    for (size_t i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) {
        char dir_name[MAX_FILENAME];
        sprintf(dir_name, "./%s", positions[i]);

        if (access(dir_name, F_OK) != 0) { // jika direktori belum ada
            create_directory(dir_name);
        }
    }

    // baca setiap entri di dalam direktori "players"
    rewinddir(dir);
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // jika entri adalah file biasa
            char* image_file = entry->d_name;
            char* ext = strrchr(image_file, '.');

            if (ext != NULL && (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".png") == 0)) { // jika file adalah file gambar
                for (size_t i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) { // cek setiap posisi pemain
                    if (strstr(image_file, positions[i]) != NULL) { // jika nama file cocok dengan posisi pemain
                        char old_path[MAX_FILENAME];
                        sprintf(old_path, "./players/%s", image_file);

                        char new_path[MAX_FILENAME];
                    sprintf(new_path, "./%s/%s", positions[i], image_file);

                    move_file(old_path, new_path, image_file);
                    break;
                }
            }
          }
        }
      }

  // hapus direktori "players"
  char* args[] = {
      "rm",
      "-r",
      "./players",
      NULL
  };
  pid_t pid = fork();

  if (pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
  } else if (pid == 0) {
      execvp(args[0], args);
      perror("execvp");
      exit(EXIT_FAILURE);
  } else {
      int status;
      waitpid(pid, &status, 0);
      if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
          printf("Error removing directory: %s\n", "./players");
          exit(EXIT_FAILURE);
      }
  }

  // tutup direktori
  if (closedir(dir) != 0) { // jika closedir() gagal
      perror("closedir");
      exit(EXIT_FAILURE);
    }
  }

void buatTim(int bek, int gelandang, int striker) {
  char * positions[] = {
    "Kiper",
    "Bek",
    "Gelandang",
    "Penyerang"
  };
  char * formasi = malloc(sizeof(char) * MAX_FILENAME);
  sprintf(formasi, "/home/salten/sisop/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  FILE * teamFile = fopen(formasi, "w");
  if (teamFile == NULL) {
    printf("Error creating team file: %s\n", strerror(errno));
    return;
  }
  fprintf(teamFile, "Formasi %d-%d-%d\n\n", bek, gelandang, striker);

  int ratings[5][20] = {
    0
  }; // Array untuk menyimpan rating semua pemain
  char playerNames[5][20][MAX_FILENAME]; // Array untuk menyimpan nama semua pemain
  int count[5] = {
    0
  }; // Array untuk menyimpan jumlah pemain di setiap posisi

  // Buka setiap folder posisi dan dapatkan rating dan nama setiap pemain
  for (int i = 0; i < 4; i++) {
    DIR * dir = opendir(positions[i]);
    if (dir == NULL) {
      printf("Error opening directory %s\n", positions[i]);
      continue;
    }
    struct dirent * entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        sprintf(fileName, "%s/%s", positions[i], entry -> d_name);
        char * rating = strrchr(entry -> d_name, '_') + 1; // dapatkan rating dari nama file
        char * name = entry -> d_name;
        name[strlen(name) - strlen(rating) - 1] = '\0'; // hapus peringkat dari nama file
        int ratingVal = atoi(rating);
        ratings[i + 1][count[i + 1]] = ratingVal;
        strcpy(playerNames[i + 1][count[i + 1]], name); // copy nama pemain ke array
        count[i + 1]++;
      }
    }
    closedir(dir);
  }

  // Dapatkan pemain terbaik untuk setiap posisi berdasarkan rating mereka
  int bestKiper = 0;
  int bestBek[bek];
  int bestGelandang[gelandang];
  int bestPenyerang[striker];

  for (int i = 0; i < count[1]; i++) {
    if (ratings[1][i] > ratings[1][bestKiper]) {
      bestKiper = i;
    }
  }

  for (int i = 0; i < bek; i++) {
    bestBek[i] = -1;
  }

  for (int i = 0; i < count[2]; i++) {
    for (int j = 0; j < bek; j++) {
      if (bestBek[j] == -1 || ratings[2][i] > ratings[2][bestBek[j]]) {
        int k;
        for (k = bek - 1; k > j; k--) {
          bestBek[k] = bestBek[k - 1];
        }
        bestBek[j] = i;
        break;
      }
    }
  }

  for (int i = 0; i < gelandang; i++) {
    bestGelandang[i] = -1;
  }

  for (int i = 0; i < count[3]; i++) {
    for (int j = 0; j < gelandang; j++) {
      if (bestGelandang[j] == -1 || ratings[3][i] > ratings[3][bestGelandang[j]]) {
        int k;
        for (k = gelandang - 1; k > j; k--) {
          bestGelandang[k] = bestGelandang[k - 1];
        }
        bestGelandang[j] = i;
        break;
      }
    }
  }

  for (int i = 0; i < striker; i++) {
    bestPenyerang[i] = -1;
  }

  for (int i = 0; i < count[4]; i++) {
    for (int j = 0; j < striker; j++) {
      if (bestPenyerang[j] == -1 || ratings[4][i] > ratings[4][bestPenyerang[j]]) {
        int k;
        for (k = striker - 1; k > j; k--) {
          bestPenyerang[k] = bestPenyerang[k - 1];
        }
        bestPenyerang[j] = i;
        break;
      }
    }
  }

  // Print pemain terbaik untuk setiap posisi ke file tim
  fprintf(teamFile, "Kiper: %s (%d)\n", playerNames[1][bestKiper], ratings[1][bestKiper]);
  fprintf(teamFile, "Bek:\n");
  for (int i = 0; i < bek; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[2][bestBek[i]], ratings[2][bestBek[i]]);
  }
  fprintf(teamFile, "Gelandang:\n");
  for (int i = 0; i < gelandang; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[3][bestGelandang[i]], ratings[3][bestGelandang[i]]);
  }
  fprintf(teamFile, "Penyerang:\n");
  for (int i = 0; i < striker; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[4][bestPenyerang[i]], ratings[4][bestPenyerang[i]]);
  }

  fclose(teamFile);
  free(formasi);
}




// Main
int main() {
    downloadFile();
    extractFile();
    filterPlayers();
    categorizePlayers();
    
    int bek, gelandang, penyerang;
    printf("Mau defend pake brp orang? ");
    scanf("%d", &bek);
    printf("Jumlah manusia ngambang? ");
    scanf("%d", &gelandang);
    printf("Siapa nih yang di depan? ");
    scanf("%d", &penyerang);
  
    printf(&bek);
    printf(&gelandang);

    buatTim(bek, gelandang, penyerang);
    return 0;
}
