#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define MAX_LEN 1024

void print_run() {
    printf("Error: Argumen tidak valid. Gunakan: ./program * HH MM SS path/to/script.sh\n");
}

int main(int argc, char *argv[]) {
    if (argc != 6) {
        print_run();
        return 1;
    }
    
    // Memisahkan argumen
    char *script_path = argv[5];
    int HH, MM, SS; // Deklarasi jam, menit, detik
    if (strcmp(argv[1], "*") == 0) {
        HH = -1;
    } 
    else {
    	// konversi argumen 1 sebagai jam (HH)
        HH = atoi(argv[1]);
        if (HH < 0 || HH > 23) {
            printf("Error: Argumen tidak valid.\n");
            return 1;
        }
    }
    // konversi argumen 2 dan 3 sebagai menit (MM) dan detik (SS)
    MM = atoi(argv[2]);
    SS = atoi(argv[3]);
    if (MM < 0 || MM > 59 || SS < 0 || SS > 59) {
        printf("Error: Argumen menit atau detik tidak valid.\n");
        return 1;
    }

    // Menghitung sleep time
    time_t now = time(NULL);
    struct tm *now_tm = localtime(&now);
    int sleep_time;
    if (HH == -1) {
        // eksekusi script setiap menit
        sleep_time = 60 - now_tm->tm_sec;
    } else {
        // eksekusi script pada waktu spesifik
        if (now_tm->tm_hour > HH || (now_tm->tm_hour == HH && now_tm->tm_min > MM) || (now_tm->tm_hour == HH && now_tm->tm_min == MM && now_tm->tm_sec >= SS)) {
            // kondisi saat waktu yang ditentukan telah berlalu, maka akan dilakukan besok
            sleep_time = 24 * 60 * 60 - (now_tm->tm_hour * 60 * 60 + now_tm->tm_min * 60 + now_tm->tm_sec) + HH * 60 * 60 + MM * 60 + SS;
        } else {
            // kondisi saat waktu yang ditentukan belum berlalu, masih pada hari yang sama
            sleep_time = (HH - now_tm->tm_hour) * 60 * 60 + (MM - now_tm->tm_min) * 60 + (SS - now_tm->tm_sec);
        }
    }
	
    // Fork proses dan jalankan script dalam proses child
    pid_t pid = fork();
    if (pid == -1) {
        printf("Error: Gagal melakukan fork.\n");
        return 1;
    } else if (pid == 0) {
        // Child process
        sleep(sleep_time);
        execlp(script_path, script_path, NULL);
        printf("Error: Gagal mengeksekusi script.\n");
        return 1;
    } else {
        // Parent process
        // background
        printf("Silakan tunggu, script akan dijalankan dalam %d detik.\n", sleep_time);
        return 0;
    }
}